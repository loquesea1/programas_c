/*Raul hernandez Lopez
 * freeenrgy1975@gmail.com
 * 14 de octubre del 2020*/

/*Una persona recibe un préstamo de $10,000.00 de un banco y desea 
 * saber cuánto pagará de interés, si el banco le cobra una tasa del
27% anual.*/

#include <stdio.h>

int main(){
	/*Declaracion de variables*/
	float tasa_interes = 0.27;
	float Monto, prestamo;
	int fecha_actual, fecha_prestamo, tiempo_trascurrido, x;	
	printf("Ingresa la cantidad de dinero que solicitaste $ ");	
	scanf( "%f", &prestamo );	
	printf("Ingresa el año de prestamo :");
	scanf( "%d", &fecha_prestamo );
	printf("Ingresa el año actual :");
	scanf( "%d", &fecha_actual);
	tiempo_trascurrido = fecha_actual - fecha_prestamo; 

	for(x = 1; x <= tiempo_trascurrido; x++){
		fecha_prestamo += 1;
		prestamo = prestamo + (prestamo * tasa_interes);
		printf("\nAño [%d] El monto a pagar es de [$%f]", fecha_prestamo, prestamo);	
	}/*fin ciclo for*/	
}/*fin metodo main*/
