/*RAÚL HERNÁNDEZ LÓPEZ
 *freeenergy1975@gmail.com
 modificada el 23 de octubre del 2020*/

/*Almacena los nombres de N alumnos y de acuerdo a la calificacion obtenida determina el numero de aprobados y reprobados*/

#include <stdio.h>

/*inicio del metodo main*/
int main(){
	/*Declaracion de variables*/
	int num_alum, aprob = 0, Naprob = 0, contador = 0, x; 
	/*Definimos el tipo de caracteres*/
	typedef char tam_nombre[30];
	
	
	printf("\nCual es el numero de alumnos a evaluar ? :");
	scanf("%d", &num_alum);
	/*Definimos nuestro arreglo*/
	tam_nombre nombre[num_alum];
	/*Definimos el arreglo que almacenara las calificaciones de los alumnos*/
	float calificacion[num_alum];
	 for(x = 0; x < num_alum; x++){
		/*Recopila datos*/
		contador = contador + 1;
		printf("\nCual es el nombre del alumno? :");
		scanf("%s", nombre[x]);
		printf("\ncual es la calificacion del alumno No. [%d] ?", contador);	
		scanf("%f", &calificacion[x]);
		/*cuenta el numero de aprobados*/
		if((calificacion[x] >= 6) & (calificacion[x] <= 10)){
			printf("\nEl alumno %s aprobo con una calificacion de [%f]", nombre[x], calificacion[x]);
			aprob = aprob +1;
		}
		/*Cuenta el numero de reprobados*/
		else if((calificacion[x] >= 0) & (calificacion[x] < 6)){
			printf("\nEl alumno [%s] no aprobo", nombre[x]);
			Naprob = Naprob + 1;
		}
	}
	/*impresion de resultados*/
	printf("\nEL NUMERO DE APROBADOS ES DE [%d]", aprob);
	printf("\nEL NUMERO DE DE REPROBADOS ES DE [%d]", Naprob);
}/*Fin del metodo main*/
