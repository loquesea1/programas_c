/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Jueves 01 de Octubre del 2020*/

/*Calcula el precio de un boleto en funcion de los kilometros si se sabe que 
 * el costo por kilometro es de 20.5*/

#include <stdio.h>

/*inicio del metodo principal*/
int main(){
	/*Declaración de variables*/
	float distancia, costo, kilometros = 20.5;
	/*Recopilacion de datos*/
	printf("A que distancia en kilometros se encuentra tu destino? :");
	scanf("%f", &distancia);
	/*Proceso para obtener el precio del boleto*/
	costo = distancia * kilometros;
	/*impresion de resultados*/
	printf("\nEl costo de tu boleto es de [$%f]\n", costo);
}/*fin del metodo main*/

