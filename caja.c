/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Miercoles 12 de agosto del 2020*/

/*Este programa nos permite calcular la cantidad de dinero que tenemos a partir de una cantidad de billetes y monedas definidos por el usuario, el valor de cada moneda y billete tambien es definida por el usuario.*/
#include <stdio.h>

int main(){
	/*declaracion de variables*/
	int variedadB, variedadM, total;
	int suma = 0; 
	int suma_M = 0; 
	int x, valor, cantidad;	
	int y, valor2, cantidad2;

	/*Aqui es donde el usuario determina la cantidad de billetes que posee*/
	printf("cuantos billetes de distinto valor tienes? :");
	scanf("%d", &variedadB);
	
	/*calcula la cantidad de dinero recabado en billetes */
	for( x = 1; x <= variedadB; x++){
		printf("Cual es el valor del billete? : $");
		scanf("%d", &valor);
	
		printf("Cuantos billetes con ese valor tienes? :");
		scanf("%d", &cantidad);
		
		suma = suma + (valor * cantidad);
	}
	
	/*El usuario determina la cantidad de monedas que posee*/
	printf("\ncuantas monedas de distinto valor tienes? :");
	scanf("%d", &variedadM);

	/*calcula la cantidad de dinero recabado en monedas*/
	for(y = 1; y <= variedadM; y++){
		printf("\nCual es el valor de la moneda? :$");
		scanf("%d", &valor2);
		
		printf("\nCuantas monedas tienes con ese valor? :");
		scanf("%d", &cantidad2);
		
		suma_M = suma_M + (valor2 * cantidad2);
	}	/*declaramos una variable que sumara la cantidad de dinero recabado en monedas y billetes y he imprime resultados*/
	total = suma + suma_M;
	printf("\nEl total de la caja registradora es de :$%d", total); 	
}
