/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  10   de  enero  del  2021
 * Tema: Calcula la sumatoria de un numeros*/

#include <stdio.h> 
int main(){
    //Declaracion de variables
    int x, sumatoria = 1, numero;

    printf("\nSumatoria de números\n");
    
    //Almacena el numero para calcular la sumatoria
    printf("\nIngresa un número entero positivo: ");
    scanf("%d", &numero);

    for( x = 1; x <= numero; x++){
    	sumatoria += x;
	printf("\nLa sumatoria de %d %s %d %s", x, "=", sumatoria , "\n");
    }//Fin for
}//Fin del metodo main 

