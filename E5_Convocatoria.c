/* Autor:Hernandez  Lopez Raul @Neo
 * correo: freeenergy1975@gmail.com
 * fecha 8 de Enero del 2021
 * TEMA:   EN    EL   TECNOLOGICO   NACIONAL  DE   MEXICO   SE  PUBLICO  UNA
 * CONVOCATORIA DE  ESTUDIOS  EN  EL  EXTRANJERO, PODRAN PARTICIPAR AQUELLOS
 * ESTUDIANTES  QUE TENGAN UN  PROMEDIO  MAYOR  A  90. LOS ALUMNOS QUE HAYAN
 * ALCANZADO LA CALIFICACION ENTRARAN  DIRECTO  A LA TOMBOLA PARA EL SORTEO.
 * LOS ALUMNOS QUE NO ALCANCEN EL PROMEDIO PODRAN PARTICIPAR EN LA SIGUIENTE
 * CONVOCATORIA SI APRUEBAN EL EXAMEN  DEL IDIOMA PARA EL SIGUIENTE SEMESTRE
 * Y MEJORAN EN UN 20% SU PROMEDIO.*/

#include <stdio.h>

int main(){
	//Declaracion de variables
	typedef char String[30];
	int numAspirantes, x, y;
	
	printf("\nPrimera convocatoria para becas en el extranjero\n");

	printf("\nNúmero de aspirantes: ");
	scanf("%d", &numAspirantes);

	/*Define el arreglo que almacenara el nombre de los estudiantes y su 
	 * calificacion.*/
	String nombreAlumnos[numAspirantes];
	String apellidoPaterno[numAspirantes];
	String apellidoMaterno[numAspirantes];
	float calificaciones[numAspirantes];
	float examenIdiomas[numAspirantes];
	float califSegSemestre[numAspirantes];
	float comparacionCalificaciones;

	for(x = 0; x < numAspirantes; x++){
	   //Entrada de datos.	
	   printf("\nNombre: ");
	   scanf("%s", &nombreAlumnos[x]);
			
	   printf("\nApellido Paterno: ");
           scanf("%s", &apellidoPaterno[x]);

	   printf("\nApellido Materno: ");
	   scanf("%s", &apellidoMaterno[x]);

	   printf("\ncalificacion '1/100': ");
	   scanf("%f", &calificaciones[x]);
	   
	   /*verifica  que  la  calificacion se encuentre  dentro del rango 
	   establecido.*/
	   if((calificaciones[x] >= 1) && (calificaciones[x] <= 100)){
	     //Determina que el alumno califique para a convocatoria.	   
	     if(calificaciones[x] > 90 ){
	     	printf("\nEl alumno :\n%s %s %s %s%s",
		       nombreAlumnos[x], apellidoPaterno[x], apellidoMaterno[x],	
	       	       "\nAplica para la convocatoria\n",	       
		       "____________________________\n");
	     }//Fin if anidado 1
	     else{
	     	printf("\nEl alumno :\n%s %s %s %s %s",
                       nombreAlumnos[x], apellidoPaterno[x], apellidoMaterno[x],       
                       "\nNo aplica para la convocatoria\n",       
                       "_______________________________\n");
	     }//Fin else.
	   }//Fin if.
	   
	   else{//El valor esta fuera de rango
	   	printf("El valor ingresado esta fuera del rango establecido");
	   }
	}//Fin for.

	printf("\nSegunda convocatoria para becas en el extranjero\n");
	
	//Recorre el arreglo
	for(y = 0; y < numAspirantes; y++){
		//determina que los alumnos sean aptos para la convocatoria
		if((calificaciones[y] <= 90) && (calificaciones[y] >= 70)){
			printf("\n%s %s %s%s", nombreAlumnos[y],
			   apellidoPaterno[y], apellidoMaterno[y],
			   "\n\nIngrese calificacion de Examen de idiomas '1/100': ");

			scanf("%f", &examenIdiomas[y]);
		    //Evalua que el alumno aprueve el examen de idiomas.
		    if(examenIdiomas[y] >= 60){
		    	printf("\nEn el semestre anterior tu calificacion fue de %f %s", 
			       calificaciones[y], "\n\nIngresa tu calificacion actual: ");
			scanf("%f", &califSegSemestre[y]);
			
			comparacionCalificaciones = calificaciones[y] + (calificaciones[y] * 0.20);
			/*Evalua si el alumno aumento el 20% de su calificacion
			 *o que tenga una calificacion mayo a 90*/ 
			if((califSegSemestre[y] > 90) || (califSegSemestre[y] >= comparacionCalificaciones)){
				printf("\nEl alumno es apto para la segunda convocatoria%s%s",
				       "\nen becas al extranjero.\n",
				       "________________________________________________\n");	
			}//Fin if anidado 2.0
			else{
				printf("\nNo aplica%s",
				       "\n_________\n");
			}//Fin else 2.0
		    }//if anidado 2
	    	    else{
		        printf("\nNo aplica",
			       "\n_________\n");
		    }//Fin else	    
		}//Fin if 2.1
	}//Fin for 2	
}//Fin metodo pincipal.


