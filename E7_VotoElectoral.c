/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Programa que determina si una persona es apta  para  votar  si es
 * mayor  de  edad,  cuenta  con  credencial  para votar, es vigente
 * y si es que se encuentra en el patron electoral de no cumplir con 
 * las especificaciones se considera como no apto.
 */

#include <stdio.h>

int main(){
   //Declaracion de variables.	
   int edad, vigencia, credencial,
       patronElectoral;

   printf("\nIngresa tu edad: ");
   scanf("%d", &edad);

   if(edad >= 18){
   	printf("\nCuenta con credencial para votar?: %s",
	       "\n1)Si\n0)No: ");
	scanf("%d", &credencial);

	if(credencial == 1){
		printf("\nFecha de vigencia por ejemplo '2001': ");
		scanf("%d", &vigencia);	

		if(vigencia >= 2006){
			printf("\nSe encuentra en el patron electoral? %s",
			       "\n1)Yes\n0)No: ");	
			scanf("%d", &patronElectoral);

			if(patronElectoral == 1){
				printf("\nEres apto para votar %s",
				       "\n____________________\n");
			}//Fin if 4
			else{
                                printf("\nNo eres apto para votar%s",
                                "\n_______________________\n");
                        }//Fin else 4
		}//Fin if 3
		else{
        	     printf("\nNo eres apto para votar%s",
               	     "\n_______________________\n");
                }//Fin else 3
	}//Fin if 2.
	else{
        printf("\nNo eres apto para votar%s",
               "\n_______________________\n");
        }//Fin else 2.
   }//Fin if 1
   else{
   	printf("\nNo eres apto para votar%s",
	       "\n_______________________\n");
   }//Fin else 1
}//Fin metodo principal.
