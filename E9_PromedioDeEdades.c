/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema:CALCULAR EL PROMEDIO DE LAS EDADES DEL GRUPO DE PRIMER SEMESTRE*/

#include <stdio.h>

int main(){
	//Declaracion de variables
	int numAlumnos, sumaEdades = 0, x;
	float promedioEdades;

	printf("\nA continuacion calcularemos el \npromedio de  edades  de  primer \nsemestre.\n");
	
	//Dtermina el tamaño del arreglo
	printf("\nNumero de edades a capturar: ");
	scanf("%d", &numAlumnos);
	int edades[numAlumnos];

	for(x = 0; x < numAlumnos; x++){
		//Captura de datos.
		printf("\n[%d%s", x+1,  "] Edad: ");
		scanf("%d", &edades[x]);
		
		sumaEdades += edades[x];
	}
	//Impresion de resultados
	promedioEdades = sumaEdades / numAlumnos;
	printf("\nEl promedio de edades del grupo es: %f%s", promedioEdades, "\n");
}//Fin metodo main.
