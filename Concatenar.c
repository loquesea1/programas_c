/*Raul_Hernandez_Lopez
freeenergy1975@gmail.com
08 octubre del 2020*/

/*Leer tres números enteros de un Digito y almacenarlos en una
sola variable que contenga a esos tres dígitos Por ejemplo
si A=5 y B=6 y C=2 entonces X=562*/

#include <string.h>
#include <stdio.h>


/*inicio del metodo main*/
int main(){
    /*Declaracion de variable*/
    char caracter_1[7], caracter_2[7], caracter_3[7];
    /*Recaba datos*/
    printf("\nIngresa el primer caracter :");
    scanf("%s", &caracter_1);
    printf("\nIngresa el segundo caracter :");
    scanf("%s", &caracter_2);
    printf("\nIngresa el tercer caracter :");
    scanf("%s", &caracter_3);
    /*la funcion strcat concatena la variable caracter_1 con la variable
    * caracter_2*/
    strcat (caracter_1, caracter_2);
    /*concatena la bariable anterior con caracter_3*/
    strcat (caracter_1, caracter_3);
    /*Imprime las cadenas que concatenamos*/
    printf("\ncadenas concatenadas :D [%s]", caracter_1);
	

}
