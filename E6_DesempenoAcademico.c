/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema: Elabora un programa con condicionales anidados que solicite 3 calificaciones, 
 * obtén el promedio  de  esas  tres  calificaciones, y de acuerdo al promedio que se 
 * obtuvo, coloca la leyenda que le corresponde*/

#include <stdio.h>

int main(){
	//Declaracion de variables
	int x;
	float calificaciones[3], promedio;
	
	//Descricion.
	printf("\nIngresa 3 calificaciones con%s %s", 
	       "\nbase  en  ello  se  definira",
	       "\ntu promedio\n");
	//Entrada de datos.
	for(x = 0; x < 3; x++){
	    printf("\n[%d%s", (x + 1), "] Calificacion '1/100': ");    
	    scanf("%f", &calificaciones[x]);
	}
	//Definicion de promedio
	promedio = (calificaciones[0] + calificaciones[1] + calificaciones[2])/3;
	//Evaluacion del promedio e impresion de resultados.
	if((promedio >= 1) && (promedio <= 100)){
	    if((promedio >= 95) && (promedio <= 100)){
		printf("\nEres Exelente!%s",
		       "\n______________\n");
	    }	
	    else if((promedio >= 85) && (promedio <= 94)){
		printf("\nEres Notable!%s",
                       "\n______________\n");
	    }
	    else if((promedio >= 75) && (promedio <= 84)){
		printf("\nEres bueno%s",
                       "\n______________\n");
	    }
	    else if((promedio >= 70) && (promedio <= 74)){
		printf("\nSuficiente%s",
                       "\n______________\n");
	    }
	    else if(promedio < 70){
		  printf("\nNA (no alcanza)%s",
                       "\n______________\n");
	    }
	} 
	//Control un posible error.
        else{
	    printf("\nEl valor ingresado esta fuera del rango establecido\n");
	}//Fin else.	
}//Fin del metodo principal
