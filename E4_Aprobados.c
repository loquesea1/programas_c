/* Autor: HERNANDEZ LOPEZ RAUL @Neo
 * correo: freeenergy1975@gmail.com
 * fecha: 08  de   enero  del  2021
 * Tema: Genera un programa que determine si la calificacion es aprobatoria o no*/

#include <stdio.h>

int main(){
    //Declaracion de variables.
    int x = 0, eleccion;
    float calificacion [50];
    typedef char nombre[20];
    nombre nombreAlumnos[50];

    printf("\nA  continuacion  se  te\n%s%s%s%s",
	   "solicitara que ingreses\n",
	   "una  calificacion en un\n",
	   "rango del 1 al 100\n",
	   "_______________________\n");

    do{
        //Entrada de datos.
        printf("\nNombre: ");
        scanf("%s", &nombreAlumnos[x]);

        printf("\nCalificacion: ");
        scanf("%f", &calificacion[x]);

	if((calificacion[x] >= 1) && (calificacion[x] <= 100)){
	    if(calificacion[x] >= 60){
		printf("\nFelicidades has aprobado!%s",
		       "\n_________________________");
	    }//Fin if 
	    else{
	    	printf("\nNo has aprobado!\n%s",
		       "_______________");
	    }//Fin else anidado
	}//Fin if

	else{
	    printf("\nEl valor ingresado no es valido\n%s%s",
                   "por favor vuelve a comenzar\n",
		   "_______________________________\n");
	    eleccion = 1;
	}//Fin else

	//Despliega un pequeño menú para volver a ejecutar el programa.
	printf("\nDeseas continuar 1)yes 0)no :"); 
	scanf("%d", &eleccion);

	x++;
    }while(eleccion == 1);
}//Fin metodo principal
