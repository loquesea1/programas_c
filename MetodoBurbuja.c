/*RAÚL HERNÁNDEZ LÓPEZ
 *freeenergy1975@gmail.com
  23 de octubre del 2020*/

/*Ordena de manera ascendente una serie de valores*/
#include <stdio.h>
/*Inicio metodo main*/
int main(){
	/*Declaracion de variables */
	int x, posicion, z, w, t, tamano, nueva_posicion;
	printf("\ncuantos datos deseas ordenar? :");
	scanf("%d", &tamano);
	t = tamano - 1 ;
	/*Declaracion del arreglo que almacenara los valores a ordenar*/
	int ordenar[tamano];
	/*Recolecta cada valor y lo almacena en el arreglo*/
	for(x = 0; x < tamano; x++){
		printf("\nIngresa el valor del elemento a ordenar :");
		scanf("%d", &ordenar[x]);
	}/*Fin ciclo for 1*/
	/*ordena los valores por medio del metodo de la burbuja*/
	for(z = 0; z < tamano ; z++){
		for(posicion = 0; posicion < t; posicion++){			
			if(ordenar[posicion] > ordenar[posicion+1]){
				/*cambia posiciones*/
				nueva_posicion = ordenar[posicion + 1];
				ordenar[posicion + 1] = ordenar[posicion];
				ordenar[posicion] = nueva_posicion;
			}/*fin if*/
		}/*fin for 3*/
	}/*fin for 2*/
	printf("\nOrden ascendente :");
	for(w = 0; w < tamano; w++){
		printf("%d", &ordenar[w]);
	}
}/*Fin metodo main*/
	


