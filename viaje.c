/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Lunes 17 de agosto del 2020*/

/*Determina el costo de un boleto de viaje de acuerdo a la distancia de recorrido y el costo por kilometro*/
#include <stdio.h>

int main(){
	/*Declaracion de variables de tipo flotante*/
	int distancia, precio, kilometros = 17;
	/*Recopilacion y Procesamiento de datos*/
	printf("A que distancia en kilometros se encuentra tu destino? : ");
	scanf("%d", &distancia);
	precio = distancia * kilometros;
	/*Impresion de resultados*/
	printf("\nEl costo de tu boleto es de : $%d %s", precio, "Buen viaje! :D\n");
}
