/*AUTOR:  HERNÁNDEZ   LÓPEZ   RAÚL
 *CORREO: freeenergy1975@gmail.com
 *FECHA : 06  de  enero  del  2021
 * */

/* TEMA :EN UNA TIENDA DEPARTAMENTAL ESTA PUBLICANDO NUEVAS OFERTAS POR
 * TEMPORADA NAVIDEÑA. SI LOS CLIENTES PRESENTAN UNA TARJETA DE CLIENTE
 * FRECUENTE. LES OTORGARAN UN DESCUENTO DEL 20% DEL TOTAL DE SU COMPRA.
 */

#include<stdio.h>

int main(){
    //Declaración de variables.
    typedef char nombre[20]; 
    int eleccion,
	 tarjeta,
	 estadoCompras,
	 x = 0;
    nombre producto[30];
    float precio[30],
    	  sumaPrecio = 0,
	  descuento = 0.80,
	  montoConDescuento;
    do{
	sumaPrecio = 0;
	do{
	  //Entrada de datos.
	  printf("\nNombre del producto: ");
	  scanf("%s", &producto[x]);
	  printf("\nPrecio del producto $");
	  scanf("%f", &precio[x]);	  
	  //Almacena el valor de la suma de todos los productos.
	  sumaPrecio += precio[x];
	  x++; 
          //Verifica que todos los productos se registren	  
	  printf("\nTerminaste de resgistrar tus compras?\n1)yes\n0)no : ");	
	  scanf("%d", &estadoCompras);

	}while(estadoCompras == 0 );//Fin do while 2
		
	printf("________________________________\n%s",
	       "\nCuentas con tarjeta de cliente frecuente?\n1)yes\n0)no : ");
	scanf("%d", &tarjeta);
	
	//Evalua si el cliente cuenta con tarjeta o no.
	if(tarjeta == 1){
	    montoConDescuento = sumaPrecio * descuento;
    	    printf("\nEl Monto a pagar es de $%f%s", montoConDescuento,
	          "\n________________________________\n");	    
	}//Fin if
	else{
	    printf("\n_______________________________%s%f%s",
	           "\nMonto a pagar es $", sumaPrecio,
		   "\n_______________________________\n");
	}//Fin else 

	//Le da la opcion al cliente de volber a ejecutar el programa.
	printf("\nDesea continuar\n1)yes\n0)no : ");
	scanf("%d", &eleccion);
    }while(eleccion == 1);//Fin do while 1	    
}//Fin metodo principal
