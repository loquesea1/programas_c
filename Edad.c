/*RAÚL_HERNÁNDEZ_LÓPEZ
* freeenrgy1975@gmail.com
* 20 de octubre del 2020años*/

/*Obtener la edad de una persona en meses, si se ingresa su edad en años y meses.
 *Ejemplo: Ingresado 3 años 4 meses debemostrar 40 meses.*/ 
#include <stdio.h>

int main (){
  /*Declaracion de variables*/
  int Meses, Edad_Meses, Años;
  /*Recopila datos*/
  printf("\nIngresa tu edad en Años :");
  scanf("%d", &Años);
  printf("\nIngresa el numero de meses restantes :");
  scanf("%d", &Meses);
  /*calcula el numero de meses en los años y los suma con los meses restantes*/
  Edad_Meses = (Años * 12) + Meses;
  /*Imprime resultados*/
  printf("\nEdad en Meses[%d]\n", Edad_Meses);
}/*fin del metodo main*/
