/*Raul hernandez lopez(Berserker)*/
/*freeenergy1975@gmail.com*/
/*Miercoles 12 de agosto del 2020*/

/*Este programa determina el costo de una llamada telefonica con vase a los minutos que dure*/
#include <stdio.h>

int main(){

	/*Decaracion de variables*/
	float minutos = 1.25, costo, tiempo;
	printf("Quieres conocer el costo de tu llamada ?\nLa tarifa de llamada telefonica por minutos es de $%f", minutos);

	/*Recopilacion de datos y proceso de calculo*/
	printf("\n\nCuántos minutos duró tu llamada? :");
	scanf("%f", &tiempo);
	costo = tiempo * minutos;
	
	/*Imprecion de resultados*/
	printf("\nEl costo de tu llamada es de :$%f %s", costo, "\n");
}
