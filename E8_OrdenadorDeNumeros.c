/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  9   de  enero  del  2021
 * Tema: Elabora un programa que compare tres numeros y los ordene de mayor a menor*/

#include <stdio.h>

int main(){
	//Declaracion de variables
	int cambioPosicion, numElements, x, y, z, v;

	printf("\nCuantos numeros ingresaras?: ");
	scanf("%d", &numElements);
	int ordenar[numElements];

	//Entrada de datos
	for(x = 0; x < numElements; x++){
	    printf("\nNumero: ");
	    scanf("%d", &ordenar[x]);
	}// Fin for

	for(y = 0; y < numElements; y++){
	     for(z = 0; z < numElements-1; z++){
		if(ordenar[z] < ordenar[z+1]){
			cambioPosicion = ordenar[z];
			ordenar[z] = ordenar[z+1];
			ordenar[z+1] = cambioPosicion;
		}//Fin if
	     }//Fin for 2	
	}//Fin for 3
	
	printf("\nEl orden es el siguiente :");
	for(v = 0; v < numElements; v++){
		printf("%d %s", ordenar[v], " " );
	}//fin for 4
	printf("\n");
}//Fin metodo principal
