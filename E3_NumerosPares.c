/*AUTOR: :  HERNÁNDEZ   LÓPEZ   RAÚL
 *CORREO: freeenergy1975@gmail.com
 *FECHA : 06  de  enero  del  2021
 **/

/* UN  PROGRAMA  SOLICITA  QUE  SEAN  CAPTURADOS  TRES   DATOS
 * NUMERICOS Y A PARTIR DE ELLOS IMPRIMIRA SI EL NUMERO ES PAR.
 **/

#include <stdio.h>

int main(){
   //Declaracion de variables.
   int cantidad,
       eleccion,
       x;
      //Imprime un mensaje que pone en contexto al usuario
      printf("\nA  continuacion  se  te solicitara que ingreses %s",
              "\npara números para determinar si son par o impar");

   do{	
       //Determina el numero de veces que se ejecutara el ciclo for
       printf ("\n\nCuantos números ingresaras? : ");
       scanf("%d", &cantidad);

       int numeros[cantidad];
       
       
       for(x = 0; x < cantidad; x++){

	   //Captura de datos
           printf("\nNúmero : ");
       	   scanf("%d", &numeros[x]);

	   //Evalua si el número es par o no.
           if(numeros[x]%2 == 0){
              printf("El número es par\n%s",
	             "_________________\n");
           }//Fin if
           else{
       	     printf("El número es impar\n%s",
		     "_________________\n");

            }//Fin else
       }//Fin for
   printf("\n\nDeseas continuar\n1)yes\n0)no : ");
   scanf("%d", &eleccion);

   }while(eleccion == 1);
}//Fin metodo principal
