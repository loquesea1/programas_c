/*RAUL_HERNANDEZ_LOPEZ
 * freeenrgy1975@gmail.com
 * 15 de octubre del 2020*/

/*Calcular el descuento y el monto a pagar por un medicamento cualquiera 
 * en una farmacia si todos los medicamentos tienen un descuento del 35%.*/

#include <stdio.h>

int main(){	
	/*Declaraion de variables*/
	float Precio_Descuento, Precio_Medicamento, Descuento;
	char Medicamento[20];
	/*Recopila datos*/
	printf("INGRESA EL NOMBRE DEL MEDICAMENTO :");
	scanf("%s", &Medicamento);
	printf("Ingresa el precio real del medicamento $");
	scanf ("%f", &Precio_Medicamento);
	Precio_Descuento = Precio_Medicamento * 0.65;
	Descuento = Precio_Medicamento - Precio_Descuento;
	/*Imprime resultados*/
	printf("%s\nDescuento [$%f]\nMonto a pagar [$%f]\n "
	, Medicamento, Descuento, Precio_Descuento );
	
	
}
