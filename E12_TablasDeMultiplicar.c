/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha:  10   de  enero  del  2021
 * Tema: Imprime la tabla de multiplicar de un numero N*/

#include <stdio.h>

int main(){
	int tabla, x, resultado;

	printf("\nIngresa la tabla de multiplicar \nque quieres que se te muestre: ");
	scanf("%d", &tabla);
	//Almacena el valor que corresponde a la tabla de multiplicar.
	printf("\nTabla de multiplicar del numero %d\n", tabla);
	
	for(x = 1; x <= 10; x++){
		//Impresion ed resultados.
		resultado = tabla * x;
		printf("\n%d%s%d%s%d", tabla, " x ", x, " = ", resultado);
	}//Fin for
	printf("\n");
}//Fin del metodo main
