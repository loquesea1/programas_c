/* Autor:Hernandez  Lopez Raul @Neo
 * correo: freeenergy1975@gmail.com
 * fecha 15 de junio del 2021*/

#include <stdio.h>
#include <math.h>
//Declaracion de una funcion.
void CoordenadasDDA(int x1, int y1, int x2, int y2);

int main(){
    int x1, x2, y1, y2;	
    /*Definicion de las coordenadas iniciales y finales*/\
    printf("\nIngresa las cordenadas, x1, y1, x2, y2\n%s", 
    "es ese orden deparados por un espacio :");	    
    scanf("%d %d %d %d", &x1, &y1, &x2, &y2);
    	
    CoordenadasDDA(x1, y1, x2, y2);
}

void CoordenadasDDA(int x1, int y1, int x2, int y2){
   int x, dx, dy;
   float  steps, incx, incy, xi, yi;
   /*Determina el diferencial en y y en x.*/
   dx = x2 - x1;
   dy = y2 - y1;
   if (fabs(dx) > fabs(dy)){
      steps = fabs(dx);
   }
   else {
      steps = fabs(dy);
   }
   /*Define el incremento para x y para y*/
   incx = dx/steps;
   incy = dy/steps;
   xi = x1; 
   yi = y1;
   for(x = 0; x <= steps; x++){
       printf("\nx :%f y :%f", round(xi), round(yi));	   
       xi+= incx;
       yi+= incy;
   }
   printf("\n");
}
