/* AUTOR:  Hernandez   Lopez   Raul
 * correo: freeenergy1975@gmail.com
 * fecha: 10   de  enero  del  2021
 * Tema: Solicita la calificacion de 3 materias y almacena la calificacion de de 5 unidades, 
 * saca el promedio de las unidades y el promedio general*/

#include <stdio.h>

int main(){
    //Declaracion de variables.
    typedef char String[35];
    String materias[3]; 
    int calificaciones[3][5], 
        grupo, 
	x, y, 
	sumaCalificaciones = 0;
    float promedio, promedioGeneral, sumaPromedio = 0;

    String nombre, 
           apellidoPaterno, 
	   apellidoMaterno, 
	   carrera;
    	
    //Recopilacion de datos.
    printf("\nNombre: ");
    scanf("%s", &nombre);

    printf("\nApellido Paterno: ");
    scanf("%s", &apellidoPaterno);

    printf("\nApellido Materno: ");
    scanf("%s", &apellidoMaterno);

    printf("\nGrupo: ");
    scanf("%d", &grupo);

    printf("\ncarrera: ");
    scanf("%s", &carrera);
    
    
    for(x = 0; x < 3; x++){

	printf("\n\nNombre de la materia: ");    
	scanf("%s", &materias[x]);

	printf("\nIngresa las calificaciones de cada unidad\n");
        for(y = 0; y < 5; y++){
	    printf("Ingresa la calificacion de la unidad %d '1/100' :", y+1);
	    scanf("%d", &calificaciones[x][y]);
	}//Fin for 2
    }//Fin for 1


    for(x = 0; x < 3; x++){

	 sumaCalificaciones = 0;

	 printf("\n\nNombre del alumno: %s %s %s %s %s %s %d %s %s %s", nombre, 
	        apellidoPaterno, apellidoMaterno, "\ncarrera: ", carrera, "\nGrupo: ", grupo, 
		"\nMateria: ", materias[x], "\n\n" );    

	 printf("U1\tU2\tU3\tU4\tU5n\n");

    	 for(y = 0; y < 5; y++){
		printf("%d  %s", calificaciones[x][y], "\t");
		sumaCalificaciones += calificaciones[x][y];
	}
	promedio = sumaCalificaciones/5; 
	sumaPromedio += promedio;
	printf("\n______________________________________________________________\nPromedio: %f", 
			promedio);
    }
    promedioGeneral = sumaPromedio/3;
    printf("\nPromedio General: %f\n", promedioGeneral);


}//Fin metodo principal.    
